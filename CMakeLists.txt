project(five-letters LANGUAGES CXX)
cmake_minimum_required(VERSION 2.8.9)

enable_testing()

set(GETTEXT_PACKAGE ${CMAKE_PROJECT_NAME})

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 14)

include(GNUInstallDirs)
if(EXISTS "/etc/debian_version")
    execute_process(
      COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
      OUTPUT_VARIABLE ARCH_TRIPLET
      OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  set(CMAKE_INSTALL_LIBDIR lib/${ARCH_TRIPLET})
  set(CMAKE_INSTALL_LIBEXECDIR "${CMAKE_INSTALL_LIBDIR}")
  set(CMAKE_INSTALL_FULL_LIBEXECDIR "${CMAKE_INSTALL_FULL_LIBDIR}")
  if(CLICK_MODE)
    set(CMAKE_INSTALL_PREFIX /)
    set(CMAKE_INSTALL_BINDIR lib/${ARCH_TRIPLET}/bin)
    execute_process(
      COMMAND dpkg-architecture -qDEB_HOST_ARCH
      OUTPUT_VARIABLE CLICK_ARCH
      OUTPUT_STRIP_TRAILING_WHITESPACE
      )
  endif(CLICK_MODE)
endif(EXISTS "/etc/debian_version")

# Sets BUILD_VERSION
execute_process(
  COMMAND git describe --tags --abbrev=0 --exact-match
  OUTPUT_VARIABLE BUILD_VERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
  ERROR_QUIET
  )
if(NOT BUILD_VERSION)
  execute_process(
    COMMAND git describe --tags --abbrev=0
    OUTPUT_VARIABLE LAST_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_QUIET
    )
  string(TIMESTAMP BUILD_VERSION "${LAST_VERSION}.0.%Y%m%d%H%M%S" UTC)
endif(NOT BUILD_VERSION)
message(STATUS "Build version is: ${BUILD_VERSION}")

find_package(Threads REQUIRED)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

find_package(Intltool REQUIRED)
intltool_merge_translations(
  ${CMAKE_PROJECT_NAME}.desktop.in
  ${CMAKE_PROJECT_NAME}.desktop
  ALL
  UTF8
)

if(CLICK_MODE)
  configure_file(manifest.json.in manifest.json)
  install(FILES
    ${CMAKE_PROJECT_NAME}.desktop
    ${CMAKE_PROJECT_NAME}.png
    ${CMAKE_PROJECT_NAME}.apparmor
    ${CMAKE_CURRENT_BINARY_DIR}/manifest.json
    DESTINATION .
  )
else(CLICK_MODE)
  install(
    FILES ${CMAKE_PROJECT_NAME}.desktop
    DESTINATION ${CMAKE_INSTALL_DATADIR}/applications
  )
  install(
    FILES ${CMAKE_PROJECT_NAME}.png
    DESTINATION ${CMAKE_INSTALL_DATADIR}/icons/hicolor/scalable/apps
  )
endif(CLICK_MODE)

add_subdirectory(src)
add_subdirectory(po)

find_package(CoverageReport)
enable_coverage_report(
  TARGETS ${CMAKE_PROJECT_NAME}
  TESTS ${COVERAGE_TEST_TARGETS}
  FILTER /usr/include ${CMAKE_BINARY_DIR}/*
)
